docker-waarneming-forum
====================

Dockerfile for simplemachines forum. 

Contents
-------------
Dockerfile
docker-entrypoint.sh
msmtprc
remoteip.conf

General
-------------
Creates container in which simplemachines forum software will be available, installed using recommendations from simplemachines forum. 
- based on php-apache buster container
- aspell spelling packages for nl, en and fr
- php gd library with png, freetype and jpeg support
- mod remoteip is used in apache since you want to know the ip of the user and not the ip of the loadbalancer, you should not run this (non ssl) container directly but usage behind reverse proxy which can offer SSL offloading is advised. Traefik for example. Edit the remoteip.conf file to define the network in which the reverse proxy is running. 17.16.0.0/12 should be good for most docker-compose based setups. Apache logging modified so it shows remote ip of user instead of remote ip of loadbalancer. 
- msmtp is used for sending email from php to mailserver. msmtprc can be modified, the entry "mailfrom@example.com" will be replace by the MAIL_FROM variable during the start of the container by the entrypoint script.
- smfinstall file exists in the container, entrypoint script only installs SMF if no Settings.php file is found in the webroot. 

Build container using CI/CD or manual
-------------
- PHP memory settings can be configured in ARG values in the dockerfile, only during docker build. 
- SMF version / download file can be configured in ARG values in the dockerfile, only during docker build. 

```
docker build -t smf .
```

Usage
-------------
Requires working database and I would strongly suggest to use traefik so SSL can be used. Using docker-compose is advised at this moment, see https://gitlab.com/naturalis/bii/waarneming/docker-compose-waarneming-forum for an example.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`
