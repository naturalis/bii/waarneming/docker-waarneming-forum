#!/bin/bash


# Download and install SMF when no settings file is found
if ! [[ -f /var/www/html/Settings.php ]] ; then

# Download SMF
  mkdir -p /var/www/html \
    && cd /var/www/html \
    && tar zxf /smfinstall.tar.gz

# Set the permissions SMF wants. They say 777 suggested!
  chmod 777 /var/www/html/attachments \
    /var/www/html/avatars \
    /var/www/html/cache \
    /var/www/html/Packages \
    /var/www/html/Packages/installed.list \
    /var/www/html/Smileys \
    /var/www/html/Themes \
    /var/www/html/agreement.txt \
    /var/www/html/Settings.php \
    /var/www/html/Settings_bak.php

fi

# Set mail from address ( replace mailfrom@example.com )
    /bin/sed -i -E "s/mailfrom@example.com/$MAIL_FROM/" "/etc/msmtprc"


# run server
/usr/sbin/apache2ctl -D FOREGROUND
